<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    $animal = new Animal("shaun");
    echo "name = " . $animal->name ."<br>";
    echo "legs = " . $animal->legs ."<br>";
    echo "cold_blooded = " . $animal->cold_blooded ."<br><br>";

    $frog = new new_frog("buduk");
    echo "name = " . $frog->name ."<br>";
    echo "legs = " . $frog->legs ."<br>";
    echo "cold_blooded = " . $frog->cold_blooded ."<br>";
    echo "jump = " . $frog->jump ."<br><br>";

    $ape = new new_ape("kera sakti");
    echo "name = " . $ape->name ."<br>";
    echo "legs = " . $ape->legs ."<br>";
    echo "cold_blooded = " . $ape->cold_blooded ."<br>";
    echo "yell = " . $ape->yell ."<br>";


?>